Risks
Although Cloud Computing is a great innovation in the world of computing, there also exist downsides of cloud
computing. Some of them are discussed below:
SECURITY & PRIVACY
It is the biggest concern about cloud computing. Since data management and infrastructure management in cloud
is provided by third-party, it is always a risk to handover the sensitive information to such providers.
Although the cloud computing vendors ensure more secure password protected accounts, any sign of security
breach would result in loss of clients and businesses.
LOCK-IN
It is very difficult for the customers to switch from one Cloud Service Provider (CSP) to another. It results in
dependency on a particular CSP for service.
ISOLATION FAILURE
This risk involves the failure of isolation mechanism that separates storage, memory, routing between the different
tenants.